variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache"
    GIT_DEPTH: "0"

cache:
    paths:
        -   venv/
        -   .tox/

stages:
    -   lint
    -   build
    -   test
    -   release

#########################      Lint Stage     #################################

lint-flake8-dlint:
    before_script:
        -   python -m pip install --quiet dlint
    cache: {}
    image: python:3.11.1-slim-bullseye
    script:
        -   python -m flake8 --select=DUO --extend-ignore=DUO102,DUO116 tests/
        -   python -m flake8 --select=DUO --extend-ignore=DUO102,DUO116
            vr_delaunay_to_voronoi/
    stage: lint

lint-doctest:
    before_script:
        -   python3.10 -m venv venv
        -   venv/bin/python3.10 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.10 -m tox -e py310 --notest
    cache: {}
    image: python:3.10-slim-bullseye
    script:
        -   .tox/py310/bin/python -m doctest -v ./tests/*.py
        -   .tox/py310/bin/python -m doctest -v ./tests/**/*.py
        -   .tox/py310/bin/python -m doctest -v ./vr_delaunay_to_voronoi/*.py
        -   .tox/py310/bin/python -m doctest -v ./vr_delaunay_to_voronoi/**/*.py
    stage: lint

lint-flake8:
    cache: {}
    image: registry.gitlab.com/pipeline-components/flake8:latest
    script:
        -   flake8 --verbose --max-line-length=80 tests/
        -   flake8 --verbose --max-line-length=80 vr_delaunay_to_voronoi/
    stage: lint

lint-isort:
    cache: {}
    image: xcgd/isort
    script:
        - isort tests/ vr_delaunay_to_voronoi/
    stage: lint

lint-mypy:
    before_script:
        -   python3.10 -m venv venv
        -   venv/bin/python3.10 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.10 -m tox -e py310 --notest
    cache: {}
    image: python:3.10-slim-bullseye
    script:
        -   .tox/py310/bin/python -m mypy tests/
        -   .tox/py310/bin/python -m mypy vr_delaunay_to_voronoi/
    stage: lint

lint-markdown:
    image:
      name: markdownlint/markdownlint:0.11.0
      entrypoint: [""]
    cache: {}
    script:
        -   mdl README.md
        -   mdl README_DEVELOPERS.md
    stage: lint

lint-sphinx:
    before_script:
        -   python3.10 -m venv venv
        -   venv/bin/python3.10 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.10 -m tox -e sphinx --notest
    cache: {}
    image: python:3.10-slim-bullseye
    script:
        -   venv/bin/python3.10 -m tox -e sphinx
    stage: lint

#########################     Build Stage     #################################

build-wheel:
    image: python:3.10-slim-bullseye
    needs:
        -   lint-markdown
        -   lint-flake8
        -   lint-flake8-dlint
        -   lint-doctest
        -   lint-isort
        -   lint-mypy
    before_script:
        -   pip install twine
    cache: {}
    script:
        -   python3 setup.py sdist bdist_wheel
        -   TWINE_USERNAME=gitlab-ci-token
            TWINE_PASSWORD=${CI_JOB_TOKEN}
            python -m twine upload
            --verbose
            --repository-url
            ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
            dist/*
    stage: build

#########################     Test stage      #################################

python38:
    artifacts:
        when: always
        reports:
            junit: report.xml
    cache: {}
    coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
    needs:
        -   build-wheel
    image: python:3.8-slim-bullseye
    before_script:
        -   python3.8 -m venv venv
        -   venv/bin/python3.8 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.8 -m tox -e py38 --notest
        -   .tox/py38/bin/python -m pip install --verbose
            --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple
            --no-deps
            vr_delaunay_to_voronoi
    script:
        -   venv/bin/python3.8 -m tox -e py38
    stage: test

python39:
    artifacts:
        when: always
        reports:
            junit: report.xml
    cache: {}
    coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
    needs:
        -   build-wheel
    image: python:3.9-slim-bullseye
    before_script:
        -   python3.9 -m venv venv
        -   venv/bin/python3.9 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.9 -m tox -e py39 --notest
        -   .tox/py39/bin/python -m pip install --verbose
            --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple
            --no-deps
            vr_delaunay_to_voronoi
    script:
        -   venv/bin/python3.9 -m tox -e py39
    stage: test

python310:
    artifacts:
        when: always
        reports:
            junit: report.xml
    cache: {}
    coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
    needs:
        -   build-wheel
    image: python:3.10-slim-bullseye
    before_script:
        -   python3.10 -m venv venv
        -   venv/bin/python3.10 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.10 -m tox -e py310 --notest
        -   .tox/py310/bin/python -m pip install --verbose
            --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple
            --no-deps
            vr_delaunay_to_voronoi
    script:
        -   venv/bin/python3.10 -m tox -e py310
    stage: test

#########################    Release stage    #################################

release-pypi:
    image: python:3.10-slim-bullseye
    needs:
        -   python38
        -   python39
        -   python310
    before_script:
        -   pip install twine
    cache: {}
    script:
        -   python3 setup.py sdist bdist_wheel
        -   TWINE_USERNAME=__token__
            TWINE_PASSWORD=${DFKI_PYPI_TOKEN}
            python -m twine upload
            --verbose
            dist/*
    stage: release

pages:
    needs:
        -   lint-sphinx
        -   python38
        -   python39
        -   python310
    artifacts:
        paths:
            - public
    before_script:
        -   python3.10 -m venv venv
        -   venv/bin/python3.10 -m pip install --quiet --upgrade -r
            requirements.d/venv.txt
        -   venv/bin/python3.10 -m tox -e sphinx --notest
    cache: {}
    image: python:3.10-slim-bullseye
    script:
        -   venv/bin/python3.10 -m tox -e sphinx
    stage: release

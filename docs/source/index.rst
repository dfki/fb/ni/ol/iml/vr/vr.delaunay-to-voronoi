Welcome to the VR.Configuration Documentation!
==============================================

.. toctree::

   README
   README_DEVELOPERS

   vr_delaunay_to_voronoi

   indices_and_tables

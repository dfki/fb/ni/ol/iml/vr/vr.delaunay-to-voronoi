from typing import List

from matplotlib.axes import Axes
from matplotlib.patches import Patch


def add_patches_to_axes(
    axes: Axes,
    polygon_patches: List[Patch],
):
    for patch in polygon_patches:
        axes.add_patch(patch)

all
rule 'MD007', :indent => 4
rule 'MD009', :br_spaces => 2
rule 'MD013', :line_length => 99
exclude_rule 'MD019'
rule 'MD029', :style => 'ordered'
rule 'MD030', :ul_single => 3, :ul_multi => 3, :ol_single => 2, :ol_multi => 2
rule 'MD046', :style => :fenced

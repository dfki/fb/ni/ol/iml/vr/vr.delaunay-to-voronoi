from typing import Union

from vr_delaunay_to_voronoi.application.points_getting import get_points

from vr_delaunay_to_voronoi.application\
    .delaunay_simplices_and_voronoi_polygons_getting import \
    get_delaunay_simplices_and_voronoi_polygons
from vr_delaunay_to_voronoi.shims.library_seeding import \
    seed_standard_library_and_numpy


def test_get_delaunay_simplices_and_voronoi_polygons_2d():
    # Arrange
    seed_standard_library_and_numpy()
    dimensionality = 2
    points: Union = get_points(dimensionality=dimensionality)

    # Act
    delaunay_simplices, voronoi_polygons = \
        get_delaunay_simplices_and_voronoi_polygons(
            points=points,
        )

    # Assert
    assert delaunay_simplices is not None
    assert voronoi_polygons is not None

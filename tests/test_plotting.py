import matplotlib.pyplot as plt
from PIL import Image, ImageChops

from vr_delaunay_to_voronoi.application.plot_preparation import prepare_plot
from vr_delaunay_to_voronoi.application.plotting import plot
from vr_delaunay_to_voronoi.paths import ROOT_DATA_TEST_PLOTTING_PATH


def test_plotting():
    # Arrange
    delaunay_simplices, voronoi_polygons, x, y = prepare_plot()

    filename = ROOT_DATA_TEST_PLOTTING_PATH / 'plot.png'

    image_original = Image.open(str(filename))

    # Act
    plot(
        delaunay_simplices=delaunay_simplices,
        voronoi_polygons=voronoi_polygons,
        x=x,
        y=y,
    )
    plt.savefig(fname=filename)

    # Assert
    image_new = Image.open(str(filename))
    difference = ImageChops.difference(image_new, image_original)
    difference_bounding_box = difference.getbbox()
    assert difference_bounding_box is None

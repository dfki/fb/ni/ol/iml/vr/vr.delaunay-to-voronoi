from scipy.spatial import Delaunay

from vr_delaunay_to_voronoi.application.points_getting import get_points
from vr_delaunay_to_voronoi.voronoi.delaunay_tesselation_getting import \
    get_delaunay_tesselation


def test_get_delaunay_tesselation_2d():
    # Arrange
    dimensionality = 2
    points = get_points(dimensionality=dimensionality)

    # Act
    delaunay_tesselation: Delaunay = get_delaunay_tesselation(points=points)

    # Assert
    assert delaunay_tesselation is not None
    assert isinstance(delaunay_tesselation, Delaunay)


def test_get_delaunay_tesselation_3d():
    # Arrange
    dimensionality = 3
    points = get_points(dimensionality=dimensionality)

    # Act
    delaunay_tesselation: Delaunay = get_delaunay_tesselation(points=points)

    # Assert
    assert delaunay_tesselation is not None
    assert isinstance(delaunay_tesselation, Delaunay)


def test_get_delaunay_tesselation_4d():
    # Arrange
    dimensionality = 4
    points = get_points(dimensionality=dimensionality)

    # Act
    delaunay_tesselation: Delaunay = get_delaunay_tesselation(points=points)

    # Assert
    assert delaunay_tesselation is not None
    assert isinstance(delaunay_tesselation, Delaunay)
